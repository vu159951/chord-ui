/* @flow */

module.exports = {
  host: process.env.NODE_HOST || 'localhost', // Define your host from 'package.json'
  port: process.env.PORT,
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'Chord UI',
    titleTemplate: 'Chord UI - %s',
    meta: [
      {
        name: 'description',
        content: 'The best chord finder website',
      },
    ],
  },
};
